# Site audit findings

Date: 2020-04-02

## GENERAL SUGGESTIONS

* the project should have README.md file with instructions about how to setup and run the project
* the project should have defined prerequisites (in README), for example, version of Node.js, version of MySQL, etc, instructions on how to install them
* `yarn start` is extremly slow, this needs to be investigated

* `Node.js` version should be updated to newer. In `packag.json` you should specify min and max version of node to avoid any problems

For example:
```jason
"engines": {
    "node": ">=13.0.0 <13.12.0",
  },
```

* almost every dependency needs to be updated, many contain security issues

Running below command will output a list of dependencies with security issues. I've saved moderate, high and critical issues in [dependencies-audit.txt](dependencies-audit.txt).

```bash
yarn audit
```

To get the full list of all outdated packages run below command or see [dependecies-outdated.txt](dependecies-outdated.txt).

```bash
yarn outdated
```

I recommend to update packages with security issues first, then you should focus on your core packages (webpack, react, apollo, redux) and then all other packages that give crucial functionalities to your app.

* Start using code linter - [Eslint](https://eslint.org/) - to ensure code quality

You already have [Eslint](https://eslint.org/) - automatic code linter - in the project. But when I run `yarn run lint:js` it outputs:
```bash
✖ 74330 problems (74041 errors, 289 warnings)
```

Gitlab allows creating CI/CD pipelines which can automatically run code linters for you.

* it's worth to start using automatic code formatter - [Prettier](https://prettier.io/). It improves code quality, makes it easier to read and understand the codebase.

## PAGE LOAD PERFORMANCE

To track the progress of page load optimizations, you can use the lighthouse tool which is built in Chrome browser. https://developers.google.com/web/tools/lighthouse
Current performance score is 31/100.

### JAVASCRIPT BUNDLES SIZE

Current Webpack configuration outputs JavaScript in multiple chunks (GOOD). The big problem is with the size of two bundles `client.js` and `vendor.js`.

* You have configured `webpack-bundle-analyzer`. It's worth using it to investigate bundle sizes. You can run it with `yarn run build:stats`.
* `vendor.js` is not minified. Configuring Webpack to correctly minify this file will reduce the size of this bundle considerably.
* `vendor.js` includes the whole `react-icons` package even if you are using just a few icons. There is no need to include extra ~250KB (gzipped). Take a look at https://github.com/react-icons/react-icons/issues/154 to find out a solution on how to decrease size.
* Most of the size of `client.js` comes from CSS styles.

It happens because of most (or maybe every) components import `common.css`. Take a look at: `InstantBook.css`, `HomeType.css`, `CheckboxListItems.css` etc. It results in tons of duplicated code.

```css
@import '../../../../../components/common.css';
```

`common.css` should include *global* CSS classes that should be available across the whole app. This file should be just imported once, in one of the root components not in every single component.

Take a look at the below graph (the result of running `yarn run build:stats`)
![client.js](client-js-bundle.png)

* It's worth to use https://github.com/webpack-contrib/mini-css-extract-plugin to extract css from JavaScript bundles completely
* configure webpack to optimize react production build: https://reactjs.org/docs/optimizing-performance.html#webpack
* assets should have Cache-control, for example, max-age

https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control

Webpack includes unique hash in the bundle name (client.b62683cb.js) which will be different when you change anything in the code. This allows setting max-age even to 1 year.
Right now, when the user makes the second request, the server returns 304 not modified code, which returns only ~270B but takes ~300ms. With cache-control configured correctly, it will take ~0ms. Cache-control can be set for all assets.

* You can use CDN to improve the speed of delivering your assets (images, js, CSS, etc). To make it work you have to configure Cache-Control as stated in the previous step. For example, you could use https://www.cloudflare.com which has a free plan.

### GRAPHQL REQUESTS
* Each route has defined multiple GraphQL queries which triggers multiple requests and SQL queries. The idea of GQL is that you should start with defining one main query per route. Only add additional GQL queries if you need to fetch more data asynchronously or when some query depends on previous results.
* Requesting `https://devsitetest.online/s` results in multiple GraphQL queries which are not necessary.

First, you are fetching search listings and then for each listing, you are making separate requests to fetch `wishListStatus`. For example, if search listing returns 10 results, you will have 11 (1 for initial search and 10 for wishListStatus) separate requests to the server. **This all should be done in one query.**

**When you render collection, you should have one main component wich will fetch all needed data and then pass it to all child components.**

![gql requests](search-gql-requests.png)

### SQL SERVER QUERIES

* there is n+1 problem with SQL queries on the server. Requesting `https://devsitetest.online/s` results in hundreds of SQL queries.

You can solve it using https://github.com/graphql/dataloader.

Resources:
* https://itnext.io/what-is-the-n-1-problem-in-graphql-dd4921cb3c1a
* https://medium.com/slite/avoiding-n-1-requests-in-graphql-including-within-subscriptions-f9d7867a257d

Server logs when requesting search page:
![server queries](search-sql-queries-only-ssr.png)

## SECURITY

* `.env` the file shouldn't contain any token or secret keys. Secrets shouldn't be part of the repository. Similar what you are already doing with `./src/config.js`

Possible solution: You can add `.env` file to `.gitignore` which makes sure that file won't be committed. Then you can create `.env.example` which will have fake values. When a developer starts working on the project, creates `.env` file locally with real values.

* you should implement field level authorization https://graphql.org/learn/authorization/

For example, a user shouldn't be able to access another user's personal details like a phone number or email.

```javascript
const UserType = new ObjectType({
  name: 'User',
  fields: {
    email: {
      type: StringType,
      resolve: (user, {}, request) => {
        if (user.id === request.user.id) {
          return user.email
        }

        return null
      }
      // ....
```

* GraphQL queries should only request data that are needed. You should go over queries and make sure that all fields are necessary.

## ARCHITECTURE

* GraphQL queries

Start with defining one main query per route. Only add additional GQL queries if you need to fetch more data asynchronously or when some query depends on previous results.

If you have to render a list of items, data should be fetched in one parent component and then passed to children. Child components **should not** fetch any extra data. This results in lots of unnecessary requests.

* It would be worth splitting backend logic with the react app. Right now everything is located under ./src/. Maybe core backend logic could be located in `./src-core` or something like that.

* It's recommended to store images in [AWS S3](https://aws.amazon.com/s3/) or [Google Cloud Storage](https://cloud.google.com/storage). Both services provide a free tier. It allows parallel updates and images are stored securely.

* You should use background jobs to handle longer requests, for example, image processing
  * Resources:
    * https://devcenter.heroku.com/articles/node-redis-workers
    * https://github.com/actionhero/node-resque
  * Overview of the flow that won't freeze an app:

          1. User uploads an image
          2. The server creates a new process (background job)
             1. background job handles file transformation (adding watermarks for example), optimization, etc
             2. background job saves the image to S3 or other storage and saves file URL to the Database
          3. The user gets quick Response
          4. The web app can load image asynchronously after it was processed by background job (for example you can use [polling](https://en.wikipedia.org/wiki/Polling_(computer_science)) for that which is supported by apollo-client)

* alternative solution for image processing - serverless functions
  * You can process (add watermark, optimize etc) images outside your own server, you can use serverless functions for that
  * Thanks to that you won't have to update current Node.js on your server
  * [AWS Lambda](https://aws.amazon.com/lambda/)
  * [Google Cloud functions](https://cloud.google.com/functions)

* Avoid coping and pasting

There are multiple occurrences of similar code that handles saving files in disk storage. For example in `fileUpload.js`, `bannerUpload.js`, `documentUpoload.js` etc. Common logic should be decoupled and extracted to separate shared modules.
